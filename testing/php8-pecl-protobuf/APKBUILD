# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php8-pecl-protobuf
_extname=protobuf
pkgver=3.15.0
pkgrel=0
pkgdesc="PHP 8 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data - PECL"
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
depends="php8-common"
makedepends="php8-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize8
	./configure --prefix=/usr --with-php-config=php-config8
	make
}

check() {
	# Test suite is not a part of pecl release.
	php8 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname.so" > $_confdir/$_extname.ini
}

sha512sums="e1e6e7aacc103291d637f427d09b35fe32d4c52fcf71463c1d44d34d42332f5eff312a8a9f3e8770d0f430d16b6d20f13c1b2cebe6024ba4c82a35f03aa9b673  php-pecl-protobuf-3.15.0.tgz"
